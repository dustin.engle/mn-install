#!/bin/bash
clear

STRING1="Make sure you double check before hitting enter! Only one shot at these!"
STRING2="If you found this helpful, please donate to NORT Donation: "
STRING3="MqatcAE1mk16iJPCuvyQnyFbkFKz7VUeCd"
STRING4="Updating system and installing required packages."
STRING5="Switching to Aptitude"
STRING6="Some optional installs"
STRING7="Starting your masternode"
STRING8="Now, you need to finally start your masternode in the following order:"
STRING9="Go to your windows wallet and from the Control wallet debug console please enter"
STRING10="startmasternode alias false <mymnalias>"
STRING11="where <mymnalias> is the name of your masternode alias (without brackets)"
STRING12="once completed please return to VPS and press the space bar"
STRING13=""
STRING14="Please Wait a minimum of 5 minutes before proceeding, the node wallet must be synced"

echo $STRING1
read -e -p "Server IP Address : " ip
read -e -p "Masternode Private Key (e.g. 384KFtZX2dPBAifrMsx5PrMLph58t6wLnFXAwKu3d3oSBn765mF # THE KEY YOU GENERATED EARLIER) : " key
read -e -p "Install Fail2ban? [Y/n] : " flagFail2Ban
read -e -p "Install UFW and configure ports? [Y/n] : " flagUFW
clear

echo $STRING2
echo $STRING13
echo $STRING3
echo $STRING13
echo $STRING4
sleep 10

# update package and upgrade Ubuntu
sudo apt-get update -y
sudo apt-get upgrade -y
sudo apt-get autoremove -y
sudo apt-get install -y build-essential
sudo apt-get install -y libtool autotools-dev autoconf automake
sudo apt-get install -y libssl-dev libevent-dev
sudo apt-get install -y libboost-all-dev
sudo apt-get install -y pkg-config
sudo add-apt-repository ppa:bitcoin/bitcoin
sudo apt update -y  
sudo apt-get install -y libdb4.8-dev
sudo apt-get install -y libdb4.8++-dev
clear

echo $STRING5
sudo apt-get install -y aptitude

#Generating Random Passwords
password=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
password2=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)

echo $STRING6
if [[ ("$flagFail2Ban" == "y" || "$flagFail2Ban" == "Y" || "$flagFail2Ban" == "") ]]; then
  cd ~
  sudo aptitude install -y fail2ban
  sudo service fail2ban restart
fi
if [[ ("$flagUFW" == "y" || "$flagUFW" == "Y" || "$flagUFW" == "") ]]; then
  sudo apt-get install flagUFW
  sudo ufw default deny incoming
  sudo ufw default allow outgoing
  sudo ufw allow ssh
  sudo ufw allow 7555/tcp
  sudo ufw enable -y
fi

#Install Straks Daemon
cd
git clone https://gitlab.com/dustin.engle/straks.git
cd straks
./autogen.sh
./configure
make
sudo make install
cd
clear

#Setting up coin
echo $STRING2
echo $STRING13
echo $STRING3
echo $STRING13
echo $STRING4
sleep 10

#Create straks.conf
echo '
rpcuser='$password'
rpcpassword='$password2'
rpcallowip=127.0.0.1
rpcport=7556
listen=1
server=1
daemon=1
promode=1
logtimestamps=1
maxconnections=256
externalip='$ip'
bind=127.0.0.1
bind='$ip'
masternodeaddr='$ip':7555
masternodeprivkey='$key'
masternode=1
' | sudo -E tee ~/.straks/straks.conf >/dev/null 2>&1
sudo chmod 0600 ~/.straks/straks.conf

#Starting coin
(
  crontab -l 2>/dev/null
  echo '@reboot sleep 30 && straksd -shrinkdebugfile'
) | crontab
(
  crontab -l 2>/dev/null
  echo '@reboot sleep 60 && straks-cli masternode start'
) | crontab
straksd -daemon
clear

echo $STRING2
echo $STRING13
echo $STRING3
echo $STRING13
echo $STRING4
sleep 10

echo $STRING7
echo $STRING13
echo $STRING8
echo $STRING13
echo $STRING9
echo $STRING13
echo $STRING10
echo $STRING13
echo $STRING11
echo $STRING13
echo $STRING12
echo $STRING14
sleep 5m

read -p "Press any key to continue... " -n1 -s
straks-cli masternode debug